<?php

namespace Stanislausk\Kalibrr\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Models\User;
use Illuminate\Http\Request;

use BackendMenu;
use Flash;

class Settings extends Controller {
  public $pageTitle = "Settings";
  public $requiredPermissions = ['stanislausk.kalibrr.plugin'];

  public function __construct () {
      parent::__construct();
      BackendMenu::setContext('Stanislausk.Kalibrr', 'settings','settings');
  }

  public function index () {

  }

  // TODO: Add authentication settings when required
  // TODO: Load default settings
}
