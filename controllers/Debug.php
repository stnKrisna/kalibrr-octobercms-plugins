<?php

namespace Stanislausk\Kalibrr\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Models\User;
use Illuminate\Http\Request;

use Stanislausk\Kalibrr\SDK\KalibrrAPI;

use BackendMenu;
use Flash;

class Debug extends Controller {
  public $pageTitle = "Debug";
  public $requiredPermissions = ['stanislausk.kalibrr.plugin.debug'];

  private $_api;

  public function __construct () {
      parent::__construct();
      BackendMenu::setContext('Stanislausk.Kalibrr', 'settings','debug');

      $this->initAPI();
  }

  public function index () {

  }

  public function onFetchJobBoard () {
    return ["status"=>1,"data"=>$this->_api->getJobBoard()];
  }

  public function onFetchJob () {
    $jobId = input('value');
    return ["status"=>1,"data"=>$this->_api->getJob($jobId)];
  }

  public function onFetchJobByCompany () {
    $companyId = input('value');
    return ["status"=>1,"data"=>$this->_api->getJobsByCompany($companyId)];
  }

  public function onFetchFeaturedJob () {
    $companyId = input('value');
    return ["status"=>1,"data"=>$this->_api->getFeaturedJobs()];
  }

  public function onFetchFastResponder () {
    $companyId = input('value');
    return ["status"=>1,"data"=>$this->_api->getFastResponders()];
  }

  private function initAPI () {
    $this->_api = new KalibrrAPI();
  }
}
