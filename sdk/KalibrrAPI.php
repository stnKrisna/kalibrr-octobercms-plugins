<?php

namespace Stanislausk\Kalibrr\SDK;

use Stanislausk\Kalibrr\SDK\KalibrrHttpClient as KalibrrClient;
use Carbon\Carbon;
use Cache;

class KalibrrAPI {
  private $_client;
  private $_cacheInterval;

  // TODO: Implement authentication when required
  public function __construct () {
    $this->setKalibrrClient();

    // Cache the result for 1 hour
    $this->_cacheInterval = Carbon::now()->addMinutes(60);
  }

  public function getJobBoard ($limit = 10, $offset = 0) {

    // Fetch the latest job board. Cache result for increased performance gain.
    $jobBoard = Cache::remember(
      'jobBoard-limit'. ($limit) .'-offset'. ($offset),
      $this->_cacheInterval,
      function () use ($limit, $offset) {
        return $this->_client->getRequest('job_board/search?offset='.$offset.'&limit='.$limit);
      }
    );

    return $jobBoard;
  }

  public function getJob ($job_id) {

    // Fetch the latest job board. Cache result for increased performance gain.
    $job = Cache::remember(
      'jobBoard-job_id'. ($job_id),
      $this->_cacheInterval,
      function () use ($job_id) {
        return $this->_client->getRequest('jobs/' . $job_id);
      }
    );

    return $job;
  }

  public function getJobsByCompany ($company_id, $limit = 10, $offset = 0) {

    // Fetch the latest job board. Cache result for increased performance gain.
    $job = Cache::remember(
      'getJobsByCompany-company' . ($company_id) . '-limit'. ($limit) .'-offset'. ($offset),
      $this->_cacheInterval,
      function () use ($company_id, $offset, $limit) {
        return $this->_client->getRequest('companies/'.$company_id.'/jobs?offset='.$offset.'&limit='.$limit);
      }
    );

    return $job;
  }

  public function getFeaturedJobs ($limit = 10) {

    // Fetch the latest job board. Cache result for increased performance gain.
    $job = Cache::remember(
      'getFeaturedJobs',
      $this->_cacheInterval,
      function () use ($limit) {
        return $this->_client->getRequest('job_board/featured?count='.$limit);
      }
    );

    return $job;
  }

  public function getFastResponders ($limit = 10, $offset = 0) {

    // Fetch the latest job board. Cache result for increased performance gain.
    $job = Cache::remember(
      'getFastResponder-limit'. ($limit) .'-offset'. ($offset),
      $this->_cacheInterval,
      function () use ($limit, $offset) {
        return $this->_client->getRequest('job_board/search?responds_fast=true&offset='.$offset.'&limit='.$limit);
      }
    );

    return $job;
  }

  public function searchJobs ($searchParam, $limit = 10, $offset = 0) {
    $searchString = '';

    foreach ($searchParam as $searchParam => $searchValue) {

      foreach ($searchValue as $key => $value) {
        $searchString .= '&' . $searchParam . '=' . $value;
      }
    }

    // Fetch the latest job board. Cache result for increased performance gain.
    $jobBoard = Cache::remember(
      'searchJobs-limit'. ($limit) .'-offset'. ($offset).'-str'. (md5($searchString)),
      $this->_cacheInterval,
      function () use ($limit, $offset, $searchString) {
        return $this->_client->getRequest('job_board/search?offset='.$offset.'&limit='.$limit.$searchString);
      }
    );

    return $jobBoard;
  }

  public function constructKalibrrJobUrl ($job) {
    $jobId = $job->id;
    $jobSlug = $job->slug;

    return $this->constructKalibrrCompanyUrl($job).'/jobs/'.$jobId.'/'.$jobSlug;
  }

  public function constructKalibrrCompanyUrl ($job) {
    $companyCode = $job->company_info->code;

    return 'https://www.kalibrr.com/c/'.$companyCode;
  }

  public function getEducationLevels () {
    return [
      [
        'display' => 'less than high school graduate',
        'value' => 100
      ],
      [
        'display' => 'high school',
        'value' => 150
      ],
      [
        'display' => 'high school graduate',
        'value' => 200
      ],
      [
        'display' => 'vocational course',
        'value' => 300
      ],
      [
        'display' => 'vocational course graduate',
        'value' => 350
      ],
      [
        'display' => 'associate’s degree',
        'value' => 400
      ],
      [
        'display' => 'associate’s degree graduate',
        'value' => 450
      ],
      [
        'display' => 'bachelor’s degree (college)',
        'value' => 500
      ],
      [
        'display' => 'bachelor’s degree graduate (college)',
        'value' => 550
      ],
      [
        'display' => 'post-graduate (masters)',
        'value' => 600
      ],
      [
        'display' => 'completed post-graduate (masters)',
        'value' => 650
      ],
      [
        'display' => 'post-graduate (PhD)',
        'value' => 700
      ],
      [
        'display' => 'completed post-graduate (PhD)',
        'value' => 750
      ],
    ];
  }

  public function getWorkExperienceLevels () {
    return [
      [
        'display' => 'Internship / OJT',
        'value' => 100
      ],
      [
        'display' => 'Fresh Grad / Entry Level',
        'value' => 200
      ],
      [
        'display' => 'Associate / Specialist',
        'value' => 300
      ],
      [
        'display' => 'Mid-Senior Level',
        'value' => 400
      ],
      [
        'display' => 'Director / Executive',
        'value' => 500
      ],
    ];
  }

  public function getJobFunctions () {
    return [
      [
        'display' => 'Accounting and Finance',
        'value' => 'Accounting+and+Finance'
      ],
      [
        'display' => 'Administration and Coordination',
        'value' => 'Administration+and+Coordination'
      ],
      [
        'display' => 'Architecture and Engineering',
        'value' => 'Architecture+and+Engineering'
      ],
      [
        'display' => 'Arts and Sports',
        'value' => 'Arts+and+Sports'
      ],
      [
        'display' => 'Customer Service',
        'value' => 'Customer+Service'
      ],
      [
        'display' => 'Education and Training',
        'value' => 'Education+and+Training'
      ],
      [
        'display' => 'General Services',
        'value' => 'General+Services'
      ],
      [
        'display' => 'Health and Medical',
        'value' => 'Health+and+Medical'
      ],
      [
        'display' => 'Hospitality and Tourism',
        'value' => 'Hospitality+and+Tourism'
      ],
      [
        'display' => 'Human Resources',
        'value' => 'Human+Resources'
      ],
      [
        'display' => 'IT and Software',
        'value' => 'IT+and+Software'
      ],
      [
        'display' => 'Legal',
        'value' => 'Legal'
      ],
      [
        'display' => 'Management and Consultancy',
        'value' => 'Management+and+Consultancy'
      ],
      [
        'display' => 'Manufacturing and Production',
        'value' => 'Manufacturing+and+Production'
      ],
      [
        'display' => 'Media and Creatives',
        'value' => 'Media+and+Creatives'
      ],
      [
        'display' => 'Public Service and NGOs',
        'value' => 'Public+Service+and+NGOs'
      ],
      [
        'display' => 'Safety and Security',
        'value' => 'Safety+and+Security'
      ],
      [
        'display' => 'Sales and Marketing',
        'value' => 'Sales+and+Marketing'
      ],
      [
        'display' => 'Sciences',
        'value' => 'Sciences'
      ],
      [
        'display' => 'Supply Chain',
        'value' => 'Supply+Chain'
      ],
      [
        'display' => 'Writing and Content',
        'value' => 'Writing+and+Content'
      ],
    ];
  }

  public function getEmploymentTypes () {
    return [
      [
        'display' => 'Full time',
        'value' => 'Full+time'
      ],
      [
        'display' => 'Part time',
        'value' => 'Part+time'
      ],
      [
        'display' => 'Freelance',
        'value' => 'Freelance'
      ],
      [
        'display' => 'Contractual',
        'value' => 'Contractual'
      ],
    ];
  }

  // TODO: Implement authentication when required
  private function setKalibrrClient () {
    $this->_client = new KalibrrClient();
    $this->_client->setClient();
  }
}
