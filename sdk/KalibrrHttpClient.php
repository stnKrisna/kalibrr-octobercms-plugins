<?php

namespace Stanislausk\Kalibrr\SDK;

use GuzzleHttp\Client as Guzzle;

class KalibrrHttpClient {
  private $_client;
  private $_kalibrrBaseApiUrl = 'https://www.kalibrr.id/api/';

  // TODO: Add authentication when required
  public function setClient () {
    $this->_client = new Guzzle([
      'base_url' => $this->_kalibrrBaseApiUrl,
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json, text/javascript',
        'Accept-Charset' => 'utf-8',
      ]
    ]);
  }

  public function getRequest ($endpoint, $data = []) {
    $res = $this->_client->request("GET",$this->_kalibrrBaseApiUrl . $endpoint, $data);

    return json_decode($res->getBody());
  }

  // TODO: Implement POST request when required
}
