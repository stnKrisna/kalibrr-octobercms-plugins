<?php
namespace Stanislausk\Kalibrr;

use Illuminate\Support\Facades\Log;
use System\Classes\PluginBase;
use Backend;
use Event;
use App;
use Config;
use Illuminate\Foundation\AliasLoader;
class Plugin extends PluginBase{


    public function pluginDetails()
    {
        return [
            'name'        => 'Kalibrr',
            'description' => 'Use Kalibrr API with ease',
            'author'      => 'Stanislaus Krisna',
            'iconSvg'     => 'plugins/stanislausk/kalibrr/kalibrr.svg',
            'homepage'    => 'https://www.stanislausk.me/',
        ];
    }


    public function registerComponents()
    {
        return [
            'stanislausk\kalibrr\Components\JobShop' => 'jobShop',
            'stanislausk\kalibrr\Components\FilterScript' => 'filterscript',
            'stanislausk\kalibrr\Components\Search' => 'search',
            'stanislausk\kalibrr\Components\FeaturedJobs' => 'featuredJobs',
            'stanislausk\kalibrr\Components\FastResponders' => 'fastResponders',
        ];
    }

    public function registerNavigation()
    {
        return [
            'settings' => [
                'label'=>'Kalibrr',
                'code'  => 'settings',
                'url'=>Backend::url('stanislausk/kalibrr/settings'),
                'iconSvg'=> 'plugins/stanislausk/kalibrr/kalibrr.svg',
                'order'=>400,
                'sideMenu' => [
                    'settings' => [
                        'label'=>'Settings',
                        'code'  => 'settings',
                        'url'=>Backend::url('stanislausk/kalibrr/settings'),
                        'icon'=>'icon-cog',
                        'order'=>400,
                    ],
                    'debug' => [
                        'label'=>'Debug',
                        'code'  => 'debug',
                        'url'=>Backend::url('stanislausk/kalibrr/debug'),
                        'icon'=>'icon-bug',
                        'order'=>401,
                    ],
                ]
            ]
        ];
    }

    public function registerPermissions()
    {
        return [
            'stanislausk.kalibrr.plugin' => [
                'label' => 'Manage the Kalibrr plugin',
                'tab' => 'Kalibrr',
                'order' => 0,
            ],
            'stanislausk.kalibrr.plugin.debug' => [
                'label' => 'Debug the Kalibrr plugin',
                'tab' => 'Kalibrr',
                'order' => 1,
            ],
        ];
    }

    public function Boot()
    {

    }
}
