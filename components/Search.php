<?php namespace Stanislausk\Kalibrr\Components;

use Cms\Classes\ComponentBase;

class Search extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Search Component',
            'description' => 'Add searchbox to the page'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
