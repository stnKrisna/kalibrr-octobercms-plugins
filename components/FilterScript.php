<?php namespace Stanislausk\Kalibrr\Components;

use Cms\Classes\ComponentBase;

class FilterScript extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Filter Frontend Script',
            'description' => 'Frontend script for filter and search functionality. Add this component after all other Kalibrr components'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
