<?php namespace Stanislausk\Kalibrr\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Kalibrr\SDK\KalibrrAPI;

class FeaturedJobs extends ComponentBase
{
    private $_api;

    public $jobs;

    public function componentDetails()
    {
        return [
            'name'        => 'Featured Jobs',
            'description' => 'Display featured jobs'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun () {
      $this->initAPI();

      $jobList = $this->getJobList();

      // Expose API response to twig
      $this->jobs = $jobList;
    }

    public function jobUrl($job) {
      return $this->_api->constructKalibrrJobUrl($job);
    }

    public function companyUrl($job) {
      return $this->_api->constructKalibrrCompanyUrl($job);
    }

    private function getJobList () {
      return $this->_api->getFeaturedJobs(4);
    }

    private function initAPI () {
      $this->_api = new KalibrrAPI();
    }
}
