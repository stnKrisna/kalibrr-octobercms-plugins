<?php namespace Stanislausk\Kalibrr\Components;

use Cms\Classes\ComponentBase;

class FastResponders extends ComponentBase
{
    private $_api;

    public function componentDetails()
    {
        return [
            'name'        => 'Fast Responders',
            'description' => 'Display jobs with fast responders'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    private function initAPI () {
      $this->_api = new KalibrrAPI();
    }
}
