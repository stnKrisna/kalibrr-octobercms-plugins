<?php namespace Stanislausk\Kalibrr\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Kalibrr\SDK\KalibrrAPI;

use Response;

class JobShop extends ComponentBase
{
    private $_api;

    // Pagination parameters
    private $offset;
    private $limit;

    public $jobs;
    public $jobCount;
    public $page;

    // Pagination
    public $pageNumber; // Current page number. Starts from 1
    public $maxPage; // Max number of page

    // Page specific parameter
    public $suggest;
    public $function_list;

    public $filterStates;

    public $requestElapseTime;

    public $httpQueryString;

    private $filterSearchMode = false;
    private $filters = ['education_level', 'work_experience', 'function', 'tenure', 'salary_interval', 'salary_gte', 'text'];

    public function componentDetails()
    {
        return [
            'name'        => 'Job Pages',
            'description' => 'Display job listings (job board and jobs from companies). Supports the Search component'
        ];
    }

    public function defineProperties()
    {
        return [
          'resultsPerPage' => [
            'title' => 'Results per page',
            'description' => 'Results to display per page',
            'type' => 'string',
            'default' => 10
          ],
          'page' => [
            'title' => 'Page',
            'description' => 'Job shop page to display',
            'type' => 'string',
            'default' => '{{:page}}'
          ],
          'slug' => [
            'title' => 'Slug',
            'description' => 'Page slug to filter results',
            'type' => 'string',
            'default' => '{{:slug}}'
          ]
        ];
    }

    public function onRun () {
      $start_time = microtime(true);

      $validPage = $this->validatePage();
      if (!$validPage)
        return $this->controller->run('404');
      else
        $this->page = $this->getPage();

      $this->suggest = $this->function_list = null;

      $this->initAPI();

      $this->limit  = intval($this->properties['resultsPerPage']);

      // Calculate result offset from page number
      if ( get('pageNumber') !== null ) {
        $this->offset = (intval(get('pageNumber')) - 1) * $this->limit;
      } else {
        $this->offset = 0;
      }

      // Update filter and set flags
      $this->updateFilterState();

      // Get job list
      $jobList = $this->getJobList();

      // Expose API response to twig
      $this->jobs       = $jobList['jobs'];
      $this->jobCount   = $jobList['count'];

      if ($this->getPage() == 'board')
        $this->suggest = $jobList['suggest'];

      if ($this->getPage() == 'company')
        $this->function_list = $jobList['function_list'];

      $this->updatePagination($this->jobCount);

      $end_time = microtime(true);
      $this->requestElapseTime = ($end_time - $start_time);
    }

    // https://gist.github.com/kottenator/9d936eb3e4e3c3e02598
    public function pagination ($c, $m, $delta = 2) {
      $current = $c;
      $last = $m;
      $left = $current - $delta;
      $right = $current + $delta + 1;
      $range = array();
      $rangeWithDots = array();
      $l = -1;

      for ($i = 1; $i <= $last; $i++) {
        if ($i == 1 || $i == $last || $i >= $left && $i < $right) {
          array_push($range, $i);
        }
      }

      for($i = 0; $i<count($range); $i++) {
        if ($l != -1) {
          if ($range[$i] - $l === 2) {
            array_push($rangeWithDots, $l + 1);
          } else if ($range[$i] - $l !== 1) {
            array_push($rangeWithDots, -1);
          }
        }

        array_push($rangeWithDots, $range[$i]);
        $l = $range[$i];
      }

      return $rangeWithDots;
    }

    public function getEducationLevels () {
      return $this->_api->getEducationLevels();
    }
    public function getWorkExperienceLevels () {
      return $this->_api->getWorkExperienceLevels();
    }
    public function getJobFunctions () {
      return $this->_api->getJobFunctions();
    }
    public function getEmploymentTypes () {
      return $this->_api->getEmploymentTypes();
    }

    private function updateFilterQueryString () {
      $queryString = '';

      foreach ($this->filters as $filter) {
        if (get($filter) == null)
          continue;

        $queryString .= $filter . '=' . str_replace(' ', '+', get($filter));
        $queryString .= '&';
      }

      $this->httpQueryString = substr($queryString, 0, -1);
    }

    private function updateFilterState () {
      foreach ($this->filters as $filter) {
        $this->filterStates[$filter] = explode(",", str_replace(' ', '+', get($filter)));

        if (get($filter) != null) {
          $this->filterSearchMode = true;
        }
      }

      $this->updateFilterQueryString();
    }

    private function updatePagination ($itemCount) {
      // Store max page number
      $this->maxPage = floor($itemCount / $this->limit) + 1;

      // Store current page number
      $this->pageNumber = intval(get('pageNumber')) == null ? 1 : intval(get('pageNumber'));
    }

    private function getJobList () {
      switch ($this->getPage()) {
        case 'company': return $this->getJobsByCompany();
        case 'job': return $this->getJobDetail();
        case 'board': return $this->getJobBoard();

        default: return null;
      }
    }

    private function validatePage () {
      if ($this->getPage() == '404')
        return false;

      return true;
    }

    private function getPage () {
      switch ($this->properties['page']) {
        case 'company':
        case 'job':
        case 'board':
          return $this->param('page');

        case false:
        case null: return 'board';

        default: return '404';
      }
    }

    private function getJobBoard () {
      if ($this->filterSearchMode) { // Using filter or search mode
        $jobList = $this->_api->searchJobs($this->filterStates, $this->limit, $this->offset);
      } else { // Default. Show all
        $jobList = $this->_api->getJobBoard($this->limit, $this->offset);
      }

      return [
        'jobs' => $jobList->jobs,
        'count' => $jobList->count,
        'suggest' => $jobList->suggest
      ];
    }

    private function getJobsByCompany () {
      $jobList = $this->_api->getJobsByCompany($this->properties['slug']);

      return [
        'jobs' => $jobList->jobs,
        'count' => $jobList->total_count,
        'function_list' => $jobList->function_list
      ];
    }

    private function getJobDetail () {
      $job = $this->_api->getJob($this->properties['slug']);

      return [
        'jobs' => $job,
        'count' => 1
      ];
    }

    private function initAPI () {
      $this->_api = new KalibrrAPI();
    }
}
